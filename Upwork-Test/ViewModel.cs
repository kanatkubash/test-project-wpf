﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Upwork_Test
{
    [Serializable]    
    public class ViewModel: INotifyPropertyChanged
    {
        public bool IsEditable {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEditable"));
            }
        }
        private bool _isEditable;
        public ObservableCollection<Row> Collection { get; set; } = new ObservableCollection<Row>();
        public Row Selected { get; set; }
        public ViewModel()
        {
            Collection.CollectionChanged += Collection_CollectionChanged;
        }
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Collection"));
        }

        public void AddInitial()
        {
            AddTTC();
            AddTTbC();
            AddT3Rb();
        }

        public void AddTTbC()
        {
            Collection.Add(new TTbC() { Text1 = "Alarm Clock" });
        }

        public void AddT3Rb()
        {
            Collection.Add(new T3Rb() { Text1 = "Security Level", Arr = new bool[] { false, false, false } });
        }

        public void Delete()
        {
            if (Selected == null)
                MessageBox.Show("no item selected");
            Collection.Remove(Selected);
        }

        public void AddTTC()
        {
            Collection.Add(new TTC() { Text1 = "Door", Text2 = "Front Door" });
        }
    }
}
