﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Upwork_Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            if (File.Exists("file"))
            {
                BinaryFormatter reader = new BinaryFormatter();
                using (var file = File.Open("file", FileMode.Open))
                {
                    this.Vm = (ViewModel)reader.Deserialize(file);
                }
            }
            else
            {
                this.Vm = new ViewModel();
                this.Vm.AddInitial();
            }
            this.DataContext = Vm;
        }

        public ViewModel Vm { get; private set; }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Vm.AddTTC();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Vm.AddTTbC();
            
        }

        private void button1_Copy_Click(object sender, RoutedEventArgs e)
        {
            Vm.AddT3Rb();
            
        }

        private void button1_Copy1_Click(object sender, RoutedEventArgs e)
        {
            Vm.Delete();
        }
        private void Click(object sender,MouseButtonEventArgs e)
        {
            ///Little hack
            if (dataGrid.SelectedItem!=(sender as DataGridRow).Item)
            {
                dataGrid.SelectedItem = (sender as DataGridRow).Item;
                (sender as DataGridRow).IsSelected = true;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            BinaryFormatter saver = new BinaryFormatter();
            using (var file = File.Open("file", FileMode.Create))
            {
                saver.Serialize(file, this.Vm);
            }
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var result=VisualTreeHelper.HitTest(dataGrid, e.GetPosition(this));
            if (result == null || result.VisualHit is ScrollViewer)
            {
                dataGrid.SelectedItem = null;
                Keyboard.ClearFocus();
            }
                
        }
    }
}

