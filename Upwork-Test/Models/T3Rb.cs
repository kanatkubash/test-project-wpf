﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upwork_Test
{
    [Serializable]
    public class T3Rb : Row, INotifyPropertyChanged
    {
        public T3Rb() { Type = Types.T3Rb; GroupName = Guid.NewGuid().ToString(); }
        public string Text1
        {
            get { return _text1; }
            set
            {
                _text1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Text1"));
            }
        }
        private string _text1;
        public bool[] Arr
        {
            get { return _arr; }
            set
            {
                _arr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("WhichChecked"));
            }
        }
        public string GroupName { get; set; }
        private bool[] _arr;
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
    }

}
