﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upwork_Test
{
    [Serializable]
    public class TTC : Row, INotifyPropertyChanged
    {
        public TTC() { Type = Types.TTC; }
        public string Text1
        {
            get { return _text1; }
            set
            {
                _text1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Text1"));
            }
        }
        private string _text1;
        public string Text2
        {
            get { return _text2; }
            set
            {
                _text2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Text2"));
            }
        }
        private string _text2;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }
        private bool _isChecked;
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
