﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upwork_Test
{
    [Serializable]
    public abstract class Row
    {
        public enum Types { TTC, TTbC, T3Rb };
        public Types Type { get; set; }
    }

}
